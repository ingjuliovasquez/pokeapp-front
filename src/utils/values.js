export const isValid = (value) =>
  value !== undefined && value !== null && value !== 0 && value !== "";
export const isUndefined = (value) => value === undefined;
export const isNull = (value) => value === null;
export const isEmptyObject = (obj) =>
  obj &&
  Object.keys(obj).length === 0 &&
  Object.getPrototypeOf(obj) === Object.prototype;

export const isValidArray = (array) =>
  !isNull(array) && !isUndefined(array) && array.length > 0;
export const fieldRequired = { field: 'Campo requerido' }