import axios from "axios";
import { getStorage } from "./storage";

export const http = axios.create({
    baseURL: "http://localhost:8000",
    headers: {
      "X-Requested-With": "XMLHttpRequest",
      'Authorization': `Bearer ${getStorage('auth')}`,
    },
    withCredentials: true,
  });