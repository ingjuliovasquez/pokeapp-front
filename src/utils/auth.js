import { getStorage } from "./storage";
import { isValid } from "./values";

export const getAccessToken = () => {
  const storage = getStorage("auth");
  return storage ?? null;
};

export const isValidAuth = () => {
  const accessToken = getAccessToken();
  return isValid(accessToken);
};
