import { Route, Routes } from "react-router";
import Login from "../pages/auth/Login";
import Register from "../pages/Auth/Register";
import RequireAuth from "../pages/auth/RequireAuth";
import Home from "../pages/Home";
import Layout from "../pages/Layout";

const AppRouter = () => (
  <Routes>
    <Route path="login" element={<Login />} />
    <Route path="/" element={<Layout />}>
      <Route element={<RequireAuth />}>
        <Route index element={<Home />} />
        <Route path="registrar" element={<Register />} />
      </Route>
    </Route>
  </Routes>
);

export default AppRouter;
