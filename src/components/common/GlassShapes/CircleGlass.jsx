import React from "react";

const CircleGlass = ({ color = "orange", measures = "", delay = false }) => {
  return (
    <div
      className={`absolute ${measures} w-65 h-65 bg-${color}-300 rounded-full mix-blend-multiply filter blur-lg opacity-900 animate-blob ${delay}`}
    ></div>
  );
};

export default CircleGlass;
