import { CircularProgress } from '@mui/material'
import React from 'react'

const Loader = () => (
    <div className="flex justify-center items-center p-20">
        <CircularProgress size={40} />
    </div>
)

export default Loader