import React from "react";

const Pagination = ({ itemsPerPage, total, paginate }) => {
  const pageNumbers = [];

  for (let i = 1; i <= Math.ceil(total / itemsPerPage); i++) {
    pageNumbers.push(i);
  }

  return (
    <nav className="mb-5 mt-10">
      <ul className="flex gap-4">
        {pageNumbers.map((number) => (
          <div
            key={number}
            className="flex h-12 font-medium rounded-full bg-gray-200"
          >
            <li
            className="w-12 md:flex justify-center items-center hidden  cursor-pointer leading-5 transition duration-150 ease-in  rounded-full bg-blue-200"
              onClick={() => {
                paginate(number);
              }}
            >
              {number}
            </li>
          </div>
        ))}
      </ul>
    </nav>
  );
};

export default Pagination;
