import { Button, Step, StepLabel, Stepper, Typography } from "@mui/material";
import React, { useState } from "react";
import {
  useForm,
  Controller,
  FormProvider,
  useFormContext,
} from "react-hook-form";
import Finalize from "../../../pages/team/Finalize";
import Team from "../../../pages/team/Team";
import TrainerForm from "../../../pages/team/TrainerForm";

function getSteps() {
  return ["Entrenador", "Equipo", "Finalizar"];
}

const LinaerStepper = () => {
  const methods = useForm({
    defaultValues: {
      name: "",
      last_name: "",
      birthdat: "",
      password: "",
    },
  });
  const [activeStep, setActiveStep] = useState(0);
  const [skippedSteps, setSkippedSteps] = useState([]);
  const steps = getSteps();

  const isStepSkipped = (step) => {
    return skippedSteps.includes(step);
  };

  const handleNext = (data) => {
    console.log(data);
    if (activeStep == steps.length - 1) {
      setActiveStep(activeStep + 1);
    } else {
      setActiveStep(activeStep + 1);
      setSkippedSteps(
        skippedSteps.filter((skipItem) => skipItem !== activeStep)
      );
    }
  };

  const handleBack = () => {
    setActiveStep(activeStep - 1);
  };

  function getStepContent(step) {
    switch (step) {
      case 0:
        return <TrainerForm />;

      case 1:
        return <Team />;
      case 2:
        return <Finalize />;
      default:
        return "Paso desconocido";
    }
  }

  // const onSubmit = (data) => {
  //   console.log(data);
  // };
  return (
    <div className="p-10">
      <Stepper alternativeLabel activeStep={activeStep}>
        {steps.map((step, index) => {
          const labelProps = {};
          const stepProps = {};
          if (isStepSkipped(index)) {
            stepProps.completed = false;
          }
          return (
            <Step {...stepProps} key={index}>
              <StepLabel {...labelProps}>{step}</StepLabel>
            </Step>
          );
        })}
      </Stepper>

      {activeStep === steps.length ? (
        <Typography variant="h3" align="center">
          Registro exitoso
        </Typography>
      ) : (
        <>
          <FormProvider {...methods}>
            <form onSubmit={methods.handleSubmit(handleNext)}>
              {getStepContent(activeStep)}

              <div className="flex justify-evenly">
                <Button disabled={activeStep === 0} onClick={handleBack}>
                  Regresar
                </Button>
                <Button variant="contained" color="primary" type="submit">
                  {activeStep === steps.length - 1 ? "Finalizar" : "Siguiente"}
                </Button>
              </div>
            </form>
          </FormProvider>
        </>
      )}
    </div>
  );
};

export default LinaerStepper;
