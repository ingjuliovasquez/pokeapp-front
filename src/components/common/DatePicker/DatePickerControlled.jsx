import { TextField } from "@mui/material";
import { DatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterMoment } from "@mui/x-date-pickers/AdapterMoment";
import React from "react";
import { Controller } from "react-hook-form";

const DatePickerControlled = ({
  name,
  label,
  control,
  rules = {},
  ...props
}) => {
  return (
    <div className="mt-4">
        <Controller
          name={name}
          control={control}
          rules={rules}
          render={({ field: { onChange, value }, fieldState: { error } }) => (
            <LocalizationProvider dateAdapter={AdapterMoment}>
              <DatePicker
                label={label}
                value={value}
                onChange={onChange}
                renderInput={(params) => (
                  <TextField sx={{width: '223px'}} {...params} />
                )}
                {...props}
              />
            </LocalizationProvider>
          )}
        />
    </div>
  );
};

export default DatePickerControlled;
