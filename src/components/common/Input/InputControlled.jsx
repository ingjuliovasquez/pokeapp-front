import { TextField } from "@mui/material";
import React from "react";
import { Controller } from "react-hook-form";

const InputControlled = ({
  name,
  label,
  type = "text",
  control,
  rules = {},
  className = "",
  ...props
}) => {
  return (
    <Controller
      name={name}
      control={control}
      rules={rules}
      render={({
        field: { onChange: onChangeField, value, onBlur },
        fieldState: { error },
      }) => (
        <div className="mt-4">
          <TextField
            id={`textfield-${name}`}
            label={label}
            color="primary"
            variant="outlined"
            fullWidth
            helperText={error && error.message}
            onChange={onChangeField}
            onBlur={onBlur}
          />
        </div>
      )}
    />
  );
};

export default InputControlled;
