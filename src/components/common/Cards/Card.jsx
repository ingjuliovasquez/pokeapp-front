import React from "react";
import BasicButton from "../Buttons/BasicButton";

const Card = ({
  title,
  description,
  buttonLabel,
  btnAction = () => {},
  img,
}) => {
  return (
    <div className="relative w-60 p-2 rounded-xl bg-base-100 shadow-xl bg-gray-50 text-indigo-600">
      <img src={img} alt="logo" className="h-40 object-cover rounded-xl" />
      <div className="p-3">
        <h2 className="font-medium text-lg">{title}</h2>
        {description && <p className="font-light text-sm">{description}</p>}
        <div className="my-3">
          <BasicButton isPrimaryGradient label={buttonLabel} onClick={btnAction} />
        </div>
      </div>
    </div>
  );
};

export default Card;
