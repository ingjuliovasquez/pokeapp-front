import React, { useCallback } from "react";

const BasicButton = ({
  label = "",
  isBorder = false,
  isCancel = false,
  isLoading = false,
  isSubmit = false,
  type = "button",
  className = "",
  onClick = () => {},
  isPrimaryGradient = false,
  ...props
}) => {
  const getClassName = useCallback(() => {
    if (isBorder) return "is-border-primary";
    if (isCancel) return "is-cancel";
    if (isPrimaryGradient) return "is-primary-gradient";
    return "is-primary";
  });

  const classNameType = getClassName();
  const classLoading = isLoading ? "is-loading" : "";
  const buttonType = isSubmit ? "submit" : type;

  return (
    <button
      type={buttonType}
      onClick={onClick}
      className={`rounded px-[20px] py-[5px] ${classNameType} ${classLoading} ${className} font-semibold transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-80`}
      {...props}
    >
      {label}
    </button>
  );
};

export default BasicButton;
