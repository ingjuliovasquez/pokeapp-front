import React from "react";

const GeneralLayout = ({children}) => {
  return (
    <div className="container">
        {children}
    </div>
  );
};

export default GeneralLayout;
