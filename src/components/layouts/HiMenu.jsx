import React from 'react'
import { UilBars } from '@iconscout/react-unicons'

const HiMenu = ({color, size}) => {
  return (
    <UilBars color={color} size={size} /> 
  )
}

export default HiMenu