import React from "react";

const MobileNav = ({ navData }) => {
  return (
    <div>
      <nav className="bg-indigo-900 w-full p-4 shadow-blue-900 shadow-xl">
        <ul className="flex flex-col gap-y-6">
          {navData.map((item, index) => {
            const { name, href } = item;

            return (
              <li key={index}>
                <a className="text-white hover:text-blue-900">{name}</a>
              </li>
            );
          })}
        </ul>
      </nav>
    </div>
  );
};

export default MobileNav;
