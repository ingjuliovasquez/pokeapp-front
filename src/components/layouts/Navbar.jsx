import React, { useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";
import PokeAppLogo from "../../assets/images/logos/4.svg";
import useLogin from "../../hooks/login/useLogin";
import { getStorage } from "../../utils/storage";

const Navbar = () => {
  const { logout } = useLogin();
  const auth = getStorage("auth");

  return (
    <nav className="fixed top-0 left-0 bg-indigo-900 w-full shadow">
      <div className="container m-auto flex justify-between items-center text-white">
        <div className="pl-8 py-4">
          <Link to='/'>
            <img
              src={PokeAppLogo}
              width={40}
              alt=""
              className="transition ease-in-out delay-150 hover:-rotate-[360deg]"
            />
          </Link>
        </div>
        <ul className="hidden md:flex items-center pr-10 text-base font-semibold cursor-pointer">
          <li className="transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-80 hover:bg-indigo-800 py-2 px-3 rounded">
            Inicio
          </li>
          <li className="transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-80 hover:bg-indigo-800 py-2 px-3 rounded">
            Equipos
          </li>
          <li className="transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-80 hover:bg-indigo-800 py-2 px-3 rounded">
            Torneos
          </li>
          {auth ? (
            <li
              onClick={logout}
              className="transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-80 hover:bg-indigo-800 py-2 px-3 rounded"
            >
              Cerrar sesión
            </li>
          ) : (
            <li className="transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-80 hover:bg-indigo-800 py-2 px-3 rounded">
              <Link to="/login">Panel Admin</Link>
            </li>
          )}
        </ul>
        <button className="block md:hidden py-3 px-4 rounded focus:outline-none "></button>
      </div>
    </nav>
  );
};

export default Navbar;
