import React, { useState } from "react";
import { redirect, useNavigate } from "react-router";
import { http } from "../../utils/http";
import { getStorage, removeStorage, setStorage } from "../../utils/storage";

const useLogin = () => {
  const navigate = useNavigate();

  const [isLogged, setIsLogged] = useState(false);
  const [formState, setFormState] = useState({
    email: "entrenador@pokemon.com",
    password: "password",
  });

  const { email, password } = formState;

  const onInputChange = ({ target }) => {
    const { name, value } = target;
    setFormState({
      ...formState,
      [name]: value,
    });
  };

  const login = async (e) => {
    e.preventDefault();
    const { data } = await http.post("/api/login", {
      email: email,
      password: password,
    });

    if (data) {
      setStorage("auth", data.token);
      setIsLogged(true);
    }
    navigate("/");
  };

  const logout = async () => {
    const data = await http.post("/api/logout");

    if (data) {
      removeStorage("auth");
      navigate("/login");
    }
  };

  const getCsrfToken = async () => {
    const csrf = await http.get("/sanctum/csrf-cookie");
  };

  return {
    getCsrfToken,
    login,
    logout,
    email,
    password,
    onInputChange,
    isLogged,
  };
};

export default useLogin;
