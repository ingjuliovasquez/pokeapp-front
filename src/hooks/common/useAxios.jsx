import React, { useEffect, useState } from "react";
import axios from "axios";
import { http } from "../../utils/http.js";

const useAxios = ({ url = null, api = null }) => {
  const [result, setResult] = useState({ isLoading: false, data: null });

  useEffect(() => {
    fetchWithAxios(api);
  }, [api]);

  const fetchWithAxios = async (api) => {
    try {
      setResult({ isLoading: true, data: null });
      const { data } = await http.get(api);
      setResult({ isLoading: false, data: data });
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    fetchData(url);
  }, [url]);

  const fetchData = async (url) => {
    try {
      setResult({ isLoading: true, data: null });
      const { data } = await axios.get(url);
      setResult({ isLoading: false, data: data });
    } catch (error) {
      console.log(error);
    }
  };

  return result;
};

export default useAxios;
