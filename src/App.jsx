import { useLayoutEffect } from "react";
import { useLocation } from "react-router";
import Home from "./pages/Home";

const Wrapper = ({ children }) => {
  const location = useLocation();
  useLayoutEffect(() => {
    document.documentElement.scrollTo(0, 0);
  }, [location.pathname]);
  return children;
};

function App() {
  return (
    <Wrapper>
      <Home />
    </Wrapper>
  );
}
export default App;
