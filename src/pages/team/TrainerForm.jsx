import React from "react";
import DatePickerControlled from "../../components/common/DatePicker/DatePickerControlled";
import InputControlled from "../../components/common/Input/InputControlled";
import { fieldRequired } from "../../utils/values";

const TrainerForm = ({ control }) => {
  return (
    <div className="container flex flex-col items-center justify-center mb-8">
      <div className="flex gap-10">
        <InputControlled control={control} name="name" label="Nombre" />
        <InputControlled
          control={control}
          name="last_name"
          label="Apellido"
          rules={fieldRequired}
        />
      </div>
      <div className="flex gap-10">
        <DatePickerControlled
          control={control}
          name="birthday"
          label="Cumpleaños"
          rules={fieldRequired}
        />
        <InputControlled
          control={control}
          name="email"
          label="Correo"
          type="email"
          rules={fieldRequired}
        />
      </div>
    </div>
  );
};

export default TrainerForm;
