import React, { useEffect, useState } from "react";
import axios from "axios";
import Pokemon from "../pokemon/Pokemon";
import Loader from "../../components/common/Loader/Loader";
import Pagination from "../../components/common/Pagination/Pagination";

const Team = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [pokemonList, setPokemonList] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [pokemonsPerPage, setPokemonsPerPage] = useState(12);

  const url = "https://pokeapi.co/api/v2/pokemon?limit=151";

  const getPokemonList = async () => {
    const pokeArray = [];
    const { data } = await axios.get(url);
    if (data.results.length) {
      setPokemonList(data.results);
    }
    setIsLoading(false);
  };

  useEffect(() => {
    getPokemonList();
  }, []);

  const indexOfLastPokemon = currentPage * pokemonsPerPage;
  const indexOfFirstPokemon = indexOfLastPokemon - pokemonsPerPage;
  const currentPokemon = pokemonList.slice(
    indexOfFirstPokemon,
    indexOfLastPokemon
  );

  const paginate = (pageNumber) => setCurrentPage(pageNumber);

  return isLoading ? (
    <Loader />
  ) : (
    <>
      <div className="grid grid-cols-4 gap-4">
        {currentPokemon.map((pokemon) => {
          return <Pokemon key={pokemon.name} url={pokemon.url} />;
        })}
      </div>
      <Pagination
        itemsPerPage={pokemonsPerPage}
        total={pokemonList.length}
        paginate={paginate}
      />
    </>
  );
};

export default Team;
