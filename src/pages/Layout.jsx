import React, { Suspense } from "react";
import Header from "../components/layouts/Header";
import Linear from "../components/common/Loader/Linear";
import { Outlet } from "react-router";

const Layout = () => {
  return (
    <div className="relative">
      <Header />
      <main className="bg-gray-200 flex min-h-screen items-center justify-center">
          <Outlet />
      </main>
    </div>
  );
};

export default Layout;
