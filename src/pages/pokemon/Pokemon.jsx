import React from "react";
import Loader from "../../components/common/Loader/Loader";
import useAxios from "../../hooks/common/useAxios";

const Pokemon = ({ url }) => {
  const state = useAxios(url);
  const { isLoading, data } = state;

  return (
    <div>
      {isLoading ? <Loader /> : (
        <div className="h-[180px] bg-white shadow-xl rounded-lg flex flex-col justify-center items-center p-3">
          <img src={data?.sprites.front_default} />
          <h2 className="font-semibold text-base">{data?.name}</h2>
        </div>
      ) }
    </div>);
};

export default Pokemon;
