import React from "react";
import Card from "../components/common/Cards/Card";
import CircleGlass from "../components/common/GlassShapes/CircleGlass";
import trainers from "../assets/images/extras/trainers.webp";
import pokedex from "../assets/images/extras/pokedex.webp";
import { useNavigate } from "react-router";
import { getStorage } from "../utils/storage";

const Home = () => {
  const navigate = useNavigate();
  const auth = getStorage("auth");
  return (
    <>
      <div className="">
        <div className="relative w-full max-w-lg">
          <CircleGlass color="pink" measures="top-0 -left-1" />
          <CircleGlass
            color="yellow"
            measures="top-0 -right-1"
            delay="animation-delay-2000"
          />
          <CircleGlass
            color="red"
            measures="-bottom-10 left-20"
            delay="animation-delay-4000"
          />
          <div className="relative flex flex-col md:flex-row gap-[20px] items-center">
            <Card
              buttonLabel="Registrate"
              description="Unete a la aventura con todo tu equipo."
              title="Registrar equipo"
              img={trainers}
              btnAction={() => {
                navigate("/registrar");
              }}
            />
            {auth ? (
              <Card
                buttonLabel="Panel de admin"
                description="Ver mis estadisticas en mi panel de adminnistración"
                title="Panel"
                img={pokedex}
              />
            ) : (
              <Card
                buttonLabel="Inicia sesión"
                description="Administra tu equipo y mira todas las estadisticas."
                title="Iniciar"
                img={pokedex}
                btnAction={() => {
                  navigate("/login");
                }}
              />
            )}
          </div>
        </div>
      </div>
    </>
  );
};

export default Home;
