import React, { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { redirect } from "react-router";
import InputControlled from "../../components/common/Input/InputControlled";
import useLogin from "../../hooks/login/useLogin";
import { getStorage } from "../../utils/storage";
import { isValid } from "../../utils/values";

const Login = () => {

  const { getCsrfToken, login, onInputChange, password, email } = useLogin();

  
  useEffect(() => {
    const token = getStorage("auth");
    if (isValid(token)) {
      redirect("/");
    } else {
      getCsrfToken();
    }
  }, []);

  return (
    <div className="bg-indigo-900 min-h-screen flex items-center justify-center">
      <div className="bg-gray-100 flex rounded-2xl shadow-lg max-w-3xl p-5 items-center">
        <div className="md:w-1/2 px-8 md:px-16">
          <h2 className="font-bold text-2xl text-indigo-800">Inicia sesión</h2>
          <p className="text-xs mt-3 text-indigo-800">
            Si ya eres parte un equipo, puedes ver tus estadisticas.
          </p>
          <form onSubmit={login} className="flex flex-col gap-4">
            <input
              className="p-2 mt-5 rounded-xl border"
              type="email"
              name="email"
              placeholder="Email"
              value={email}
              onChange={onInputChange}
            />
            <div className="relative">
              <input
                className="p-2 rounded-xl border w-full"
                type="password"
                name="password"
                placeholder="Password"
                value={password}
                onChange={onInputChange}
              />
            </div>
            <button className="bg-indigo-800 rounded-xl text-white py-2 hover:scale-105 duration-300">
              Login
            </button>
          </form>

          <div className="mt-5 text-xs border-b border-indigo-800 py-4 text-indigo-800">
            <a href="#">¿Olvidaste tu contraseña?</a>
          </div>

          <div className="mt-3 text-xs flex justify-between items-center text-indigo-800">
            <p>¿No tienes un equipo?</p>
            <button
              type="submit"
              className="py-2 px-5 bg-white text-indigo-800 border rounded-xl hover:scale-110 duration-300"
            >
              Registrate
            </button>
          </div>
        </div>
        <div className="md:block hidden w-1/2">
          <img
            className="rounded-2xl"
            src="https://images.unsplash.com/photo-1605979257913-1704eb7b6246?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80"
          />
        </div>
      </div>
    </div>
  );
};

export default Login;
