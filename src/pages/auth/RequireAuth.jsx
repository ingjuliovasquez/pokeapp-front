import React from "react";
import { Navigate, Outlet, useLocation } from "react-router";
import { isValidAuth } from "../../utils/auth";

const RequireAuth = () => {
  const location = useLocation();
  const isValid = isValidAuth()

  return isValid ? (
    <Outlet />
  ) : (
    <Navigate to="/login" state={{ from: location }} replace />
  );
};

export default RequireAuth;
