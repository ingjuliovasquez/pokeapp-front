import React from "react";
import Stepper from "../../components/common/Stepper/Stepper";

const Register = () => (
  <div className="mx-auto shadow-xl rounded-2xl mt-[25px] pb-2 bg-white">
    <div className="container horizontal mt-5">
      <Stepper />
    </div>
  </div>
);

export default Register;
